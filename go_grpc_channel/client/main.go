package main

import (
	"bufio"
	"flag"
	"fmt"
	srv "go_grpc_channel/service/broadcast"
	"os"

	"log"
	"sync"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var client srv.BroadcastClient
var wait *sync.WaitGroup

var channelId = ""
var userName = ""
var userID = ""

func init() {

	var userNameStr = flag.String("name", "ametory", "your username")
	var userIdStr = flag.String("user-id", "123", "your userID")
	var channelIDstr = flag.String("channel-id", "abcded", "connect to channel")
	flag.Parse()

	userName = *userNameStr
	userID = *userIdStr

	channelId = *channelIDstr
	wait = &sync.WaitGroup{}
}

func connect(user *srv.User) error {

	var streamerror error

	stream, err := client.CreateStream(context.Background(), &srv.Connect{
		User:      user,
		Active:    true,
		ChannelId: channelId,
	})

	if err != nil {
		return fmt.Errorf("connection failed: %v", err)
	}

	wait.Add(1)
	go func(str srv.Broadcast_CreateStreamClient) {
		defer wait.Done()

		for {
			msg, err := str.Recv()
			if err != nil {
				streamerror = fmt.Errorf("Error reading message: %v", err)
				break
			}

			fmt.Printf("[%s@%s] : %s\n", msg.User.Name, channelId, msg.Content)

		}
	}(stream)

	return streamerror
}

func main() {
	timestamp := time.Now()
	done := make(chan int)

	flag.Parse()

	// id := sha256.Sum256([]byte(timestamp.String() + userName))

	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Couldnt connect to service: %v", err)
	}

	client = srv.NewBroadcastClient(conn)
	user := &srv.User{
		Id:   userID,
		Name: userName,
	}

	connect(user)

	wait.Add(1)
	go func() {
		defer wait.Done()

		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			msg := &srv.Message{
				Id:        user.Id,
				Content:   scanner.Text(),
				Timestamp: timestamp.String(),
				ChannelId: channelId,
				User:      user,
			}

			_, err := client.BroadcastMessage(context.Background(), msg)
			if err != nil {
				fmt.Printf("Error Sending Message: %v", err)
				break
			}
		}

	}()

	go func() {
		wait.Wait()
		close(done)
	}()

	<-done
}
