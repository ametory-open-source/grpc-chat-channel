import 'package:equatable/equatable.dart';
import 'package:test_flutter_grpc/protos/service.pb.dart';

abstract class ChatEvent extends Equatable {
  const ChatEvent();

  @override
  List<Object> get props => [];
}

class ChatConnect extends ChatEvent {}

class ChatMessage extends ChatEvent {
  final Message msg;

  ChatMessage(this.msg);
}
