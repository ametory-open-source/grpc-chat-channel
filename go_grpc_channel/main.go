package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	srv "go_grpc_channel/service/broadcast"
	"log"
	"net"
	"os"
	"sync"
	"time"

	_ "github.com/lib/pq"
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
	glog "google.golang.org/grpc/grpclog"
)

var grpcLog glog.LoggerV2
var DB *sql.DB

func init() {
	grpcLog = glog.NewLoggerV2(os.Stdout, os.Stdout, os.Stdout)
}

const (
	connStr = "postgresql://postgres:balakutak@127.0.0.1/chat?sslmode=disable"
)

type Connection struct {
	stream    srv.Broadcast_CreateStreamServer
	id        string
	active    bool
	channelId string
	error     chan error
}

type Server struct {
	Connection []*Connection
}

func (s *Server) CreateStream(pconn *srv.Connect, stream srv.Broadcast_CreateStreamServer) error {
	conn := &Connection{
		stream:    stream,
		id:        pconn.User.Id,
		channelId: pconn.ChannelId,
		active:    true,
		error:     make(chan error),
	}

	s.Connection = append(s.Connection, conn)
	rows, err := DB.Query("select count(*) from channels where id = $1", pconn.ChannelId)
	defer rows.Close()

	if err != nil {
		fmt.Println("ERROR DB", err)
	}

	var res int

	for rows.Next() {
		rows.Scan(&res)
	}

	if res == 0 {
		_, err := DB.Exec("insert into channels(id, timestamp) values ($1, $2)", pconn.ChannelId, time.Now())
		if err != nil {
			fmt.Println("ERROR DB", err)
		}

	}

	// CHECK USER
	countUser, err := DB.Query("select count(*) from users where id = $1", pconn.User.Id)
	defer countUser.Close()
	var countU int
	for countUser.Next() {

		countUser.Scan(&countU)
	}

	if countU == 0 {
		_, err := DB.Exec("insert into users(id, name) values ($1, $2)", pconn.User.Id, pconn.User.Name)
		if err != nil {
			fmt.Println("ERROR DB", err)
		}
	}

	// GET LAST CHAT

	msgs, err := DB.Query("select m.id, m.content , m.timestamp, m.channel_id, m.user_id , u.name  from messages m join users u on u.id = m.user_id  where m.channel_id = $1 limit 10", pconn.ChannelId)
	defer msgs.Close()
	if err != nil {
		fmt.Println("ERROR DB", err)
	}

	for msgs.Next() {
		var id, content, timestamp, channelID, userID, userName string
		msgs.Scan(&id, &content, &timestamp, &channelID, &userID, &userName)
		err := conn.stream.Send(&srv.Message{
			Id:        id,
			Content:   content,
			Timestamp: timestamp,
			ChannelId: channelID,
			User: &srv.User{
				Id:   userID,
				Name: userName,
			},
		})
		if err != nil {
			fmt.Println("ERROR DB", err)
		}
	}

	return <-conn.error
}

func (s *Server) BroadcastMessage(ctx context.Context, msg *srv.Message) (*srv.Close, error) {
	wait := sync.WaitGroup{}
	done := make(chan int)

	for _, conn := range s.Connection {
		wait.Add(1)

		go func(msg *srv.Message, conn *Connection) {
			defer wait.Done()

			if conn.active && msg.ChannelId == conn.channelId {
				err := conn.stream.Send(msg)
				grpcLog.Info("Sending message to: ", conn.stream)

				if err != nil {
					grpcLog.Errorf("Error with Stream: %v - Error: %v", conn.stream, err)
					conn.active = false
					conn.error <- err
				}

				if conn.id == msg.User.Id {
					timestamp := time.Now()
					id := sha256.Sum256([]byte(timestamp.String()))
					_, err = DB.Exec("insert into messages(id, content, timestamp, channel_id, user_id) values ($1, $2, $3, $4, $5)", hex.EncodeToString(id[:]), msg.Content, timestamp, msg.ChannelId, msg.User.Id)
					if err != nil {
						fmt.Println("ERROR", err)
					}
				}

			}
		}(msg, conn)

	}

	go func() {
		wait.Wait()
		close(done)
	}()

	<-done
	return &srv.Close{}, nil
}

func main() {
	db, err := sql.Open("postgres", connStr)
	DB = db
	if err != nil {
		log.Fatalf("error creating the server %v", err)
		return
	}
	var connections []*Connection

	server := &Server{connections}

	grpcServer := grpc.NewServer()
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("error creating the server %v", err)
	}

	grpcLog.Info("Starting server at port :8080")

	srv.RegisterBroadcastServer(grpcServer, server)
	grpcServer.Serve(listener)
}
