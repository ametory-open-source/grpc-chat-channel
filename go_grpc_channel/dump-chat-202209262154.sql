--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

-- Started on 2022-09-26 21:54:50 WIB

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE chat;
--
-- TOC entry 3262 (class 1262 OID 96372)
-- Name: chat; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE chat WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'C';


ALTER DATABASE chat OWNER TO postgres;

\connect chat

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 96385)
-- Name: channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.channels (
    id character varying,
    "timestamp" timestamp with time zone
);


ALTER TABLE public.channels OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 96373)
-- Name: messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messages (
    id character varying,
    content character varying,
    "timestamp" timestamp without time zone,
    channel_id character varying,
    user_id character varying
);


ALTER TABLE public.messages OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 96379)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id character varying,
    name character varying
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 3256 (class 0 OID 96385)
-- Dependencies: 202
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.channels VALUES ('BOD', '2022-09-26 20:50:09.955316+07');
INSERT INTO public.channels VALUES ('FINANCE', '2022-09-26 20:50:39.745875+07');


--
-- TOC entry 3254 (class 0 OID 96373)
-- Dependencies: 200
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.messages VALUES ('as2sssdsd', 'hallo', '2022-09-26 21:04:29.45025', 'BOD', 'asjhdasjasdas');
INSERT INTO public.messages VALUES ('6135c05e94e0303d95b635d220d16d562493b83def3b8e88e99ada536eb239be', 'oi', '2022-09-26 21:45:57.468422', 'BOD', 'amt123');
INSERT INTO public.messages VALUES ('5f354d8d85ed13f0bb62f15d92a155d8095971146d5787626fadfc3c5db43744', 'test', '2022-09-26 21:49:43.803547', 'BOD', 'asjhdasjasdas');
INSERT INTO public.messages VALUES ('dc67ff80023fce713c03e431f93ffdc3f13f0ae4fb7253af8dc5316411ec6391', 'asoy ini', '2022-09-26 21:49:54.089585', 'BOD', 'amt123');


--
-- TOC entry 3255 (class 0 OID 96379)
-- Dependencies: 201
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES ('asjhdasjasdas', 'aswal');
INSERT INTO public.users VALUES ('amt123', 'amet suramet');


--
-- TOC entry 3123 (class 1259 OID 96392)
-- Name: channels_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX channels_id_idx ON public.channels USING btree (id);


--
-- TOC entry 3121 (class 1259 OID 96393)
-- Name: messages_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX messages_id_idx ON public.messages USING btree (id);


--
-- TOC entry 3122 (class 1259 OID 96391)
-- Name: users_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_id_idx ON public.users USING btree (id);


-- Completed on 2022-09-26 21:54:51 WIB

--
-- PostgreSQL database dump complete
--

