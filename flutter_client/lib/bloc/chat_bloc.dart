import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:grpc/grpc.dart';
import 'package:test_flutter_grpc/protos/service.pbgrpc.dart';

import 'chat_event.dart';
import 'chat_state.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc() : super(ChatUninitialized()) {
    BroadcastClient client = BroadcastClient(
      ClientChannel(
        "127.0.0.1",
        port: 8080,
        options: ChannelOptions(
          credentials: ChannelCredentials.insecure(),
        ),
      ),
    );
    on<ChatConnect>(
      (event, emit) async {
        final resp = client.createStream(Connect(
            user: User(
              id: "12345",
              name: "FLUTTER USER",
            ),
            channelId: "BOD",
            active: true));
        emit(ChatConnected(client));
        await emit.onEach(resp, onData: (msg) => add(ChatMessage(msg)));
      },
      transformer: restartable(),
    );
    on<ChatMessage>(
      (event, emit) async {
        emit(ChatLoading());
        emit(ChatLoaded(event.msg));
      },
    );
  }

  @override
  void onEvent(ChatEvent event) {
    super.onEvent(event);
    print(event);
  }

  @override
  void onChange(Change<ChatState> change) {
    super.onChange(change);
    print("ChatBloc onChange $change");
  }

  @override
  void onTransition(Transition<ChatEvent, ChatState> transition) {
    super.onTransition(transition);
    print("ChatBloc onTransition $transition");
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    print('ChatBloc onError $error, $stackTrace');
    super.onError(error, stackTrace);
  }
}
