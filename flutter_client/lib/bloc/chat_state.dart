import 'package:equatable/equatable.dart';

import '../protos/service.pbgrpc.dart';

abstract class ChatState extends Equatable {
  const ChatState();

  @override
  List<Object> get props => [];
}

class ChatUninitialized extends ChatState {}

class ChatLoading extends ChatState {}

class ChatConnected extends ChatState {
  final BroadcastClient client;

  ChatConnected(this.client);
}

class ChatLoaded extends ChatState {
  final Message resp;

  ChatLoaded(this.resp);
}
