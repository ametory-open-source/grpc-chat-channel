import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_flutter_grpc/protos/service.pbgrpc.dart';
import 'package:uuid/uuid.dart';

import 'bloc/chat_bloc.dart';
import 'bloc/chat_event.dart';
import 'bloc/chat_state.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BlocProvider(
          create: (BuildContext context) => ChatBloc()..add(ChatConnect()),
          child: MyHomePage(title: 'Flutter Demo Home Page'),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _controller = TextEditingController();
  late Set<Message> messages;
  int _counter = 0;
  BroadcastClient? client;

  @override
  void initState() {
    messages = Set();
  }

  void addMessage(String msg) {
    if (client != null) {
      client!.broadcastMessage(Message(
          timestamp: DateTime.now().toIso8601String(),
          user: User(
            id: "12345",
            name: "FLUTTER USER",
          ),
          content: msg,
          channelId: "BOD",
          id: Uuid().v4()));
      _controller.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChatBloc, ChatState>(
        listener: (BuildContext context, ChatState state) {
      if (state is ChatConnected) {
        setState(() {
          client = state.client;
        });
      }
      if (state is ChatLoaded) {
        messages.add(state.resp);
        // print(state.resp);
        setState(() {});
      }
    }, child: BlocBuilder<ChatBloc, ChatState>(builder: (context, state) {
      return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Expanded(
                    child: ListView.builder(
                  reverse: true,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    final list = messages.toList().reversed.toList();
                    final e = list[index];
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(width: 80, child: Text(e.user.name)),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(child: Text(e.content))
                        ],
                      ),
                    );
                  },
                  itemCount: messages.length,
                )),
                Container(
                  padding: EdgeInsets.all(5),
                  child: TextField(
                    controller: _controller,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      // filled: true,
                      // fillColor: Colors.red,
                      hintText: "Type message and press enter ...",
                      hintStyle: TextStyle().copyWith(color: Colors.black45),
                    ),
                    autofocus: true,
                    onSubmitted: (val) {
                      addMessage(val);
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      );
    }));
  }
}
